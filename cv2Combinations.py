#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 21:43:05 2018

@author: user // DavidCJKennedy
"""

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
from os import listdir
from os.path import isfile, join

folderPath = ""





pilImg = Image.open("captcha.jpg").convert("RGB") 
img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

kernel = np.ones((2,2), np.uint8)

filteredImg = cv2.bilateralFilter(img, 15, 200, 200)
gray = cv2.cvtColor(filteredImg,cv2.COLOR_BGR2GRAY)
ret,otsuThresholdedImg = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
filteredImg2 = cv2.bilateralFilter(otsuThresholdedImg, 3, 200, 200)
ret2,otsuThresholdedImg2 = cv2.threshold(filteredImg2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

#dilationMorph = cv2.dilate(otsuThresholdedImg2, kernel, iterations=1)
closeMorphImg = cv2.morphologyEx(otsuThresholdedImg2, cv2.MORPH_CLOSE, kernel)
edgeDetectedImg = cv2.Canny(closeMorphImg, 250, 250)

titles = ["Original", "Edited1", "Edited2","Edited3", "Edited4", "Final"]
images = [img, filteredImg, otsuThresholdedImg,filteredImg2, closeMorphImg, edgeDetectedImg]

for i in range(6): 
    plt.subplot(2,3,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()