#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 15:23:00 2018

@author: user // DavidCJKennedy
"""

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

pilImg = Image.open("captcha.jpg").convert("RGB") 

img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

ret1,th1 = cv2.threshold(img,120,255,cv2.THRESH_BINARY) # Manual so useless
th2 = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,3) # Too much noise remains, erodes edges
th3 = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,3) # More noise than mean?!, erodes edges 
ret4,th4 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) # Far less noise than previous two
ret5,th5 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_TOZERO) # Less noise of high intensity potentially could add filtering for good response


blur2 = cv2.medianBlur(gray, 3)
blur = cv2.GaussianBlur(gray, (3,3), 0)
ret8,th8 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) # Best reponse, minimal noise. Worth trying tozero with a filter/blur


titles = ["Original Image", "Manual Binary Threshold", "Gaussian Thresholding", "Otsu Thresholding"]
images = [img, th1, th3, th8]
print(ret4)


for i in range(4):
    plt.subplot(2,2,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()
fig = plt.figure()
fig.savefig("output")