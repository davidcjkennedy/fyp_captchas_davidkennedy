#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 19:21:13 2018

@author: user // DavidCJKennedy
"""

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

###### Try on different pre-processing techniques

pilImg = Image.open("captcha.jpg").convert("RGB") 
img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

###### mess with 3rd arg as that is the contours -1 === all 

blur = cv2.GaussianBlur(gray, (3,3), 0)
ret1,th1 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret2,th2 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

ret,th = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret4,th4 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret5,th5 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret6,th6 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret7,th7 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret8,th8 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

image1, contours1, heirarchy1 = cv2.findContours(th1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
image2, contours2, heirarchy2 = cv2.findContours(th1, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
image3, contours3, heirarchy3 = cv2.findContours(th1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
image4, contours4, heirarchy4 = cv2.findContours(th1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) #### Use RETR_TREE

contourImg1 = cv2.drawContours(th1, contours1, 0, (0,255,0), 2) #### Shows basic outline misses a lot of detail, combination with sobel? 
contourImg2 = cv2.drawContours(th2, contours2, 0, (0,255,0), 2) 
contourImg3 = cv2.drawContours(th3, contours3, 0, (0,255,0), 2)
contourImg4 = cv2.drawContours(th4, contours4, 1, (0,255,0), 2) #### Collects most info on heirarchy, best results 
contourImg5 = cv2.drawContours(th5, contours1, 0, (0,255,0), 2)
contourImg6 = cv2.drawContours(th6, contours2, 0, (0,255,0), 2)
contourImg7 = cv2.drawContours(th7, contours3, 0, (0,255,0), 2)
contourImg8 = cv2.drawContours(th8, contours4, 0, (0,255,0), 2)


titles = ["1", "2","3", "4","5", "6","7", "8"]
images = [th, contourImg1, contourImg2, contourImg3, contourImg4, contourImg5, contourImg6, contourImg7, contourImg8]

for i in range(9): 
    plt.subplot(3,3,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()