#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 18:09:35 2018

@author: user // DavidCJKennedy
"""

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

pilImg = Image.open("captcha.jpg").convert("RGB") 

img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

blur1 = cv2.blur(gray, (3,3))
blur2 = cv2.GaussianBlur(gray, (3,3), 0)
blur3 = cv2.medianBlur(gray, 3)
blur4 = cv2.bilateralFilter(gray, 5, 150, 150) #Uses gaussian filter (space domain) and filter for pixel intensities should preserve edges more
blur5 = cv2.bilateralFilter(img, 15, 200, 200) #Args -- (Image, filter diameter, 5 = speed, 9 = heavy filter, sigma values, higher = strong filter effect)

###### Bilat on colour images works well, "smudges" noise and reduces intensity, keeps edges sharp and text colour intense
###### Apply progressive levels of filtering but at different grid dimensions?? 

titles = ["Original", "Average blur", "Gaussian", "Median", "Bilat - Grayscale", "Bilat - Colour"]
images = [img, blur1, blur2, blur3, blur4, blur5]

for i in range(6):
    plt.subplot(2,3,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()