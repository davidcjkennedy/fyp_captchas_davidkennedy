#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 20:40:20 2018

@author: user // DavidCJKennedy
"""

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

pilImg = Image.open("captcha.jpg").convert("RGB") 
img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

###### ----  REQ binary image 
###### Dilate = If one pixel under kernal is === 1 then make pixel 1, erode = if all pixels under kernel === 1 then make 1  

kernel = np.ones((2,2), np.uint8)
kernel2 = np.ones((2,2), np.uint8)

# %% Get binary image, both blur and regular, OTSU found best from previous tests

blur = cv2.GaussianBlur(gray, (3,3), 0)
ret1,th1 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret2,th2 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

# %% Add morphs to blured image

erosion1 = cv2.erode(th1, kernel, iterations=1) # Poor at keeping holes inside digits, mashes letters together 
dilation1 = cv2.dilate(th1, kernel, iterations=1) # useful for separating combined digits 
opening1 = cv2.morphologyEx(th1, cv2.MORPH_OPEN, kernel) # Good reponse, but because of erosion doesnt keep detail inside
closing1 = cv2.morphologyEx(th1, cv2.MORPH_CLOSE, kernel) # Best reponse, keeps detail inside and adds some separation

# %% Add morphs to non-blurred image

erosion2 = cv2.erode(th1, kernel, iterations=1)
dilation2 = cv2.dilate(th1, kernel, iterations=1)
opening2 = cv2.morphologyEx(th1, cv2.MORPH_OPEN, kernel)
closing2 = cv2.morphologyEx(th1, cv2.MORPH_CLOSE, kernel)

morph1 = cv2.morphologyEx(th2, cv2.MORPH_BLACKHAT, kernel2) # Nope
morph2 = cv2.morphologyEx(th2, cv2.MORPH_ELLIPSE, kernel2) # Nope
morph3 = cv2.morphologyEx(th2, cv2.MORPH_TOPHAT, kernel2) # Nope
morph4 = cv2.morphologyEx(th2, cv2.MORPH_CROSS, kernel2) # Potential but makes digits too thin and loses detail on already thin digits
morph5 = cv2.morphologyEx(th2, cv2.MORPH_GRADIENT, kernel2) # Nope

# %%

titles = ["Original", "Erode blur", "dilate blur", "open blur", "close blur", "Erode", "dilate", "open", "close","blackHat", "ellipse", "Tophat", "cross", "gradient"]
images = [th1, erosion1, dilation1, opening1, closing1, erosion2, dilation2, opening2, closing2, morph1, morph2, morph3, morph4, morph5]

for i in range(14):
    plt.subplot(3,5,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()