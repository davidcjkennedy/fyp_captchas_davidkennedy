#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 21:40:41 2018

@author: user // DavidCJKennedy
"""

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

###### Try on different pre-processing techniques
###### Possible to locate initial and final edge of each digit to pull individual parts, use sobel X?? 


pilImg = Image.open("captcha.jpg").convert("RGB") 
img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

canny1 = cv2.Canny(gray, 250, 750) ## If pix gradient higher than upper threshold then accepted as edge, if below lower then rejected. If between only accepted if next to pixel that is abover upper

titles = ["Original", "Basic canny"]
images = [gray, canny1]

for i in range(2): 
    plt.subplot(1,2,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()
