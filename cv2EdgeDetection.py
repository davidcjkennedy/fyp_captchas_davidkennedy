#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 21:12:23 2018

@author: user // DavidCJKennedy
"""

###### Laplacian = pants, sobel otsu+blur = potential, sobel x + Tozero = potential

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

pilImg = Image.open("captcha.jpg").convert("RGB") 
img = np.array(pilImg)
img = img[:, :, ::-1].copy() 
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

# %% Get binary image, both blur and regular, OTSU found best from previous tests

blur = cv2.GaussianBlur(gray, (3,3), 0)
ret1,th1 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret2,th2 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret3,th3 = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_TOZERO)

# %% Apply edge detection methods

laplacian = cv2.Laplacian(gray, cv2.CV_64F)
sobelX = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=3)
sobelY = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=3)

laplacian1 = cv2.Laplacian(gray, cv2.CV_64F)
sobelX1 = cv2.Sobel(th1, cv2.CV_64F, 1, 0, ksize=3)
sobelY1 = cv2.Sobel(th1, cv2.CV_64F, 0, 1, ksize=3)

laplacian2 = cv2.Laplacian(gray, cv2.CV_64F)
sobelX2 = cv2.Sobel(th2, cv2.CV_64F, 1, 0, ksize=3)
sobelY2 = cv2.Sobel(th2, cv2.CV_64F, 0, 1, ksize=3)

laplacian3 = cv2.Laplacian(gray, cv2.CV_64F)
sobelX3 = cv2.Sobel(th3, cv2.CV_64F, 1, 0, ksize=3)
sobelY3 = cv2.Sobel(th3, cv2.CV_64F, 0, 1, ksize=3)

# %% Plot

titles = ["Original", "Laplacian", "sobel x", "sobel y", "Laplacian OTSUB", "sobel x OTSUB", "sobel y OTSUB", "Laplacian OTSU", "sobel x OTSU", "sobel y OTSU", "Laplacian TOZERO", "sobel x TOZERO", "sobel y TOZERO"]
images = [gray, laplacian, sobelX, sobelY, laplacian1, sobelX1, sobelY1, laplacian2, sobelX2, sobelY2, laplacian3, sobelX3, sobelY3]

for i in range(13):
    plt.subplot(3,5,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()
