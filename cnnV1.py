# CNN - V1/Test/Simple with minimal layers

from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator

# Init CNN
classifier = Sequential()

# Convolution layer, start with 32 convolution layers, CPU = slow :(, run backend on GPU?
# Kernal size of 3x3, fixed img size of 64*64 and 3 dimensions because colour images
# args opposite way round to theano, relu to remove -ve pixels = non-linearity
classifier.add(Convolution2D(32, 3, 3, input_shape = (64, 64, 3), activation = 'relu'))

# Pooling layer, with pooling size 2x2
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Second conv layer
classifier.add(Convolution2D(64, 3, 3, activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Flattening layer
classifier.add(Flatten())

# Full connection = ANN, mes1s with output dimensions around 100 good but 2^n
classifier.add(Dense(output_dim = 128, activation = 'relu'))
# Using SIGM as binary output but need to use softmax when using more characters
classifier.add(Dense(output_dim = 1, activation = 'sigmoid'))

# Compiler
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Fit CNN to images using keras, makes easy stops overfitting
train_dataGenerator = ImageDataGenerator(rescale = 1./255, shear_range = 0.2, zoom_range = 0.2, horizontal_flip = True) 
test_dataGenerator = ImageDataGenerator(rescale = 1./255)
training_set = train_dataGenerator.flow_from_directory('dataset/training_set', target_size = (64, 64), batch_size = 32, class_mode = 'binary')
test_set = test_dataGenerator.flow_from_directory('dataset/test_set', target_size = (64, 64), batch_size = 32, class_mode = 'binary')
classifier.fit_generator(training_set, samples_per_epoch = 8000, nb_epoch = 25, validation_data = test_set, nb_val_samples = 2000)


