#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 01:49:23 2018

@author: user // DavidCJKennedy
"""

import matplotlib.pyplot as plt
import numpy as np
import sklearn.datasets as skData
import sklearn.linear_model as skModel

# %% Generate data, made up of two classes

np.random.seed(3)
X, y = skData.make_moons(200, noise=0.20)
plt.scatter(X[:,0], X[:,1], s=40, c=y, cmap=plt.cm.Spectral)

# %% Train regression classifier using class from sci-kit learn

regressClf = skModel.LogisticRegressionCV()
regressClf.fit(X,y)

# %% Variables req for gradient descent and NN, used to minimise loss (maximise prob of training)
######### Look at SGD or minibatch gradient descent, work better + decay over time #############

numExamples = len(X) # Training set size
nnInputSize = 2 # Number of inputs (X,Y)
nnOutputSize = 2 # Number of outputs (X,Y)

epsilon = 0.01 # Learning rate for gradient descent
regLambda = 0.01 # Regularisation strength

# %% Func for Tanh activation func 

def tanhActivationFunc(W1, b1, W2, b2, x):
    z1 = x.dot(W1) + b1
    a1 = np.tanh(z1)
    z2 = a1.dot(W2) + b2
    expScores = np.exp(z2)
    probability = expScores / np.sum(expScores, axis=1, keepdims=True)
    return probability

# %% Func for RELU activation func 

def relu(x):
    y = x
    for i in range(x.shape[0]):
        y[i] = np.maximum(0,x[i])
    return y

def reluDerivative(x):
    y = x
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            if(x[i][j]>0.0):
                y[i][j] = 1.0
            else:
                y[i][j] = 0.0
    return y

# %% Func for BackPropagation
    
def backPropagation(probability, model, a1, W1, W2, b1, b2):
    
    delta3 = probability
    delta3[range(numExamples), y] -= 1
    dW2 = (a1.T).dot(delta3)
    db2 = np.sum(delta3, axis=0, keepdims=True)
    delta2 = delta3.dot(W2.T) * (1 - np.power(a1, 2))
    dW1 = np.dot(X.T, delta2)
    db1 = np.sum(delta2, axis=0)
    
    # Add regularisation terms
    dW2 += regLambda * W2
    dW1 += regLambda * W1
    
    # Gradient descent params update
    W1 += -epsilon * dW1
    b1 += -epsilon * db1
    W2 += -epsilon * dW2
    b2 += -epsilon * db2
    
    model = { "W1": W1, "b1": b1, "W2": W2, "b2": b2}
    return model
    

# %% Func to plot class decision boundary on graph, generate contour plot

def generateContourPlot(boundaryPredictionFunc):
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5 
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5 
    h = 0.01  # Grid X axis distance
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h)) 
    
    # Predict the value of the func for the whole graph
    Z = boundaryPredictionFunc(np.c_[xx.ravel(), yy.ravel()]) 
    Z = Z.reshape(xx.shape)
    
    # Plot contour and generated data
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral) 
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
      
# %% Func to calculate total loss on dataSet, W1, b1, W2, b2 params need to learn from training data

def calculateLoss(model):
    W1, b1, W2, b2 = model['W1'], model['b1'], model['W2'], model['b2']
    
    probability = tanhActivationFunc(W1, b1, W2, b2, X)
    
    # Calculate Loss
    correctLogProbability = -np.log(probability[range(numExamples), y])
    dataLoss = np.sum(correctLogProbability)
    
    # Add reglarisation to loss
    dataLoss += regLambda/2 * (np.sum(np.square(W1)) + np.sum(np.square(W2))) 
    return 1./numExamples * dataLoss 

# %% Func to predict an output (0 or 1)
    
def predict(model, x):
    W1, b1, W2, b2 = model["W1"], model["b1"], model["W2"], model["b2"]
    probability = tanhActivationFunc(W1, b1, W2, b2, x)
    return np.argmax(probability, axis=1)

# %% Func to train NN, returns the model 

def trainNN(nnHiddenNodes, numPasses=20000, printLoss=False):
    model = {}
    
    # Init NN paramas to random values, layer has array of biases
    np.random.seed(0)
    W1 = np.random.randn(nnInputSize, nnHiddenNodes) / np.sqrt(nnInputSize)
    b1 = np.zeros((1, nnHiddenNodes))
    W2 = np.random.randn(nnHiddenNodes, nnOutputSize) / np.sqrt(nnHiddenNodes)
    b2 = np.zeros((1, nnOutputSize))
 
    for epoch in range(0, numPasses):
        z1 = X.dot(W1) + b1
        a1 = np.tanh(z1)
        z2 = a1.dot(W2) + b2
        expScores = np.exp(z2)
        probability = expScores / np.sum(expScores, axis=1, keepdims=True)
        model = backPropagation(probability, model, a1, W1, W2, b1, b2)
        if printLoss and epoch % 1000 == 0:
          print("Loss after iteration %i: %f" %(epoch, calculateLoss(model)))
          
    return model

# %% Plot class decision boundary on data

generateContourPlot(lambda x: regressClf.predict(x))
plt.title("Logistic Regression")

# %% Build the model with 3 dimension hidden layer
    
model = trainNN(3, printLoss=True)
generateContourPlot(lambda x: predict(model, x))
plt.title("Decision boundary for hidden layer size 3")