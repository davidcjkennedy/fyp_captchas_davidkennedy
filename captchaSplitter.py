# Img preprocessing works in current form, could increase efficiency, comments not final. Final result is an array of separated characters for each captcha img

import cv2.cv2 as cv2
import numpy as np
from os import listdir
from os.path import isfile, join
from matplotlib import pyplot as plt
from skimage import measure

# Determine types of Captcha
# Add in logic
# Add in angle rule
# Test it!
# No more after tomorrow


class ImageProcessingMethods:
    
    def segmentationPrep(self, image):
        monochromePreProcessedImg = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        ret3, threshPreProcessedImg = cv2.threshold(monochromePreProcessedImg, 0, 255, 0)
        return threshPreProcessedImg
    
    def heavyNoiseRemovalFilter(self, image):
        kernal = np.ones((2, 2), np.uint8)
        
        dst = cv2.fastNlMeansDenoisingColored(image, None, 20, 20, 5, 16) # Remove noise
        morphImg = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernal) # Remove noise within characters + further noise removal for bg
        imgGray = cv2.cvtColor(morphImg, cv2.COLOR_RGB2GRAY) # Convert Img to grayscale for best thresholding results
        ret1, otsuThreshImg = cv2.threshold(imgGray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU) # Otsu threshold img to pull foreground from background
        return otsuThreshImg
             
    
    def connectedCharacterSeperation(self, image):
        kernal = np.ones((2, 2), np.uint8)
        
        ret2, threshConnectedImg = cv2.threshold(images[i], 220, 255, cv2.THRESH_TOZERO)
        morphConnectedImg = cv2.morphologyEx(threshConnectedImg, cv2.MORPH_OPEN, kernal)
        dilatedConnectedImg = cv2.erode(morphConnectedImg, kernal, iterations = 1)
        deNoisedConnectedImg = cv2.fastNlMeansDenoisingColored(dilatedConnectedImg, None, 20, 20, 5, 16)
        ret4, threshFinalImg = cv2.threshold(deNoisedConnectedImg, 0, 255, cv2.THRESH_BINARY)
        morphImgFinal = cv2.morphologyEx(threshFinalImg, cv2.MORPH_CLOSE, kernal)
        return morphImgFinal
    
    def lineNoiseCharacterSeperation(self, image):
        kernalVert = np.ones((1, 5), np.uint8)
        kernalHoriz = np.ones((5, 1), np.uint8)
        #kernalNoise = np.ones((3, 3), np.uint8)
        
        morphedImgY = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernalVert)
        monoImgY = cv2.cvtColor(morphedImgY, cv2.COLOR_RGB2GRAY)
        ret5, otsuThreshImgY = cv2.threshold(monoImgY, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        
        morphedImgX = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernalHoriz)
        monoImgX = cv2.cvtColor(morphedImgX, cv2.COLOR_RGB2GRAY)
        ret6, otsuThreshImgX = cv2.threshold(monoImgX, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        
        mask = np.zeros(image.shape, dtype = 'uint8')
        rows = image.shape[0]
        cols = image.shape[1]
        
        for xVal in range(0, rows):
            for yVal in range(0, cols):
                mask[xVal, yVal] = 255
                if otsuThreshImgY[xVal, yVal] == 0:
                    mask[xVal, yVal] = 0
                if otsuThreshImgX[xVal, yVal] == 0:
                    mask[xVal, yVal] = 0
        
        #grayImg = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
        #_, bitConversionThresh = cv2.threshold(grayImg, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

        #openMorph = cv2.morphologyEx(bitConversionThresh, cv2.MORPH_OPEN, kernalNoise, iterations = 2)
        #sureBg = cv2.dilate(openMorph, kernalNoise, iterations = 4)
                
        #distance = cv2.distanceTransform(openMorph, 3, 5)
        #distance2 = cv2.normalize(distance, None, 255,0, cv2.NORM_MINMAX, cv2.CV_8UC1)
        #ret, sureFg =  cv2.threshold(distance2, 0.7*distance.max(), 255, 0)
        
        #sureFg = np.uint8(sureFg)
        #unknownRegion = cv2.subtract(sureBg, sureFg)
        #_, markers = cv2.connectedComponents(sureFg)
        #markers = markers + 1
        #markers[unknownRegion == 255] = 0
        
        #rgbImg = cv2.cvtColor(grayImg, cv2.COLOR_GRAY2BGR)
        
        #markers = cv2.watershed(rgbImg, markers)
        #rgbImg[markers == -1] = [255, 0, 0]        
        return mask
    
class ImageSegmentationMethods:
    
    def getCharacters(self, image):
        charsList = []
        chars = measure.label(image, neighbors = 8, background = 1)
        
        for char in np.unique(chars):
            if char == 1:
                continue
            
            mask = np.zeros(image.shape, dtype = 'uint8')
            mask[chars == char] = 255
            contImg, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            if len(contours) > 0:
                primaryContour = max(contours, key = cv2.contourArea)
                (X, Y, width, height) = cv2.boundingRect(primaryContour)
            
                # Calculate Solidarity and hieght ratio of character to determine whether is truely a character or just noise/hole
                solidarity = (cv2.contourArea(primaryContour) / float(width * height)) # Ratio between contour size and size of character, if large then probably a character
                heightRatio = height / float(image.shape[0]) # RAtio between height of character and height of original image, if large then probably character
                widthRatio = width / float(image.shape[1])
            
                verifiedSolidarity = solidarity > 0.15
                verifiedHeight = heightRatio > 0.6 
                
                # add in angle check
                
                # Probability with errors?
                
                if verifiedHeight and verifiedSolidarity:
                    charObjects = np.zeros(image.shape, dtype = 'uint8') # Create blank 2D array to store generated character hull 
                    splitObject = np.zeros(image.shape, dtype = 'uint8') # Create blank 2D array to store separated captcha character
                
                    # Generate hull for each character
                    hull = cv2.convexHull(primaryContour)
                    cv2.drawContours(charObjects, [hull], -1, 255, -1)
                
                    rows = charObjects.shape[0]
                    cols = charObjects.shape[1]
                    
                    for xVal in range(0, rows):
                        for yVal in range(0, cols):
                            if (chars[xVal, yVal] * charObjects[xVal, yVal]) == char*255:
                                splitObject[xVal, yVal] = charObjects[xVal, yVal]
                    
                    charsList.append(splitObject)
        
        
        return chars, charsList
        
    
    def sortCharacters(self, chars):
        
        for i in range(len(chars)):
            image = chars[i]            
            rows = image.shape[0]
            cols = image.shape[1]
            
            xStart = 0
            xEnd = 0
            foundStart = False
            foundEnd = False
            
            for yVal in range(0, cols):
                for xVal in range(0, rows):
                    if image[xVal, yVal] == 255 and foundStart == False:
                        xStart = yVal
                        foundStart = True
                        
            for yVal in range(cols - 1, 0, -1):
                for xVal in range(0, rows):
                    if image[xVal, yVal] == 255 and foundEnd == False:
                        xEnd = yVal
                        foundEnd = True
            
            mask = np.zeros([image.shape[0], xEnd - xStart], dtype = 'uint8')
            
            for yVal in range(0, cols):
                for xVal in range(0, rows):
                    if image[xVal, yVal] == 255:
                        trueY = yVal - xStart - 1
                        #print('Start', xStart)
                        #print('Width', xEnd - xStart)
                        #print(yVal, xVal, trueY)
                        mask[xVal, trueY] = 255

            
            maskWidthRatio = mask.shape[1] / cols
            
            if maskWidthRatio > 0.9:
                print('remove')
            elif maskWidthRatio > 0.70:
                print('chop 5')
            elif maskWidthRatio > 0.60:
                print('chop 4')
            elif maskWidthRatio > 0.40:
                xCrop = int(mask.shape[1] / 3)
                cropMask1 = mask[0:(mask.shape[0] + 1), 0:(xCrop + 1)]
                cropMask2 = mask[0:(mask.shape[0] + 1), xCrop:(xCrop*2 + 1)]
                cropMask3 = mask[0:(mask.shape[0] + 1), xCrop*2:(xCrop*3 + 1)]
                
                plt.imshow(cropMask1)
                plt.show()
                plt.imshow(cropMask2)
                plt.show()
                plt.imshow(cropMask3)
                plt.show()
                
                print('chop 3')
            elif maskWidthRatio > 0.25:
                xCrop = int(mask.shape[1] / 2)
                cropMask1 = mask[0:(mask.shape[0] + 1), 0:(xCrop + 1)]
                cropMask2 = mask[0:(mask.shape[0] + 1), xCrop:(mask.shape[1]+1)]
                
                plt.imshow(cropMask1)
                plt.show()
                plt.imshow(cropMask2)
                plt.show()
                print('chop 2')
                            
            
            
            
    
filesList = [f for f in listdir('./captchasLines/') if isfile(join('./captchasLines/', f))]
images = np.empty(len(filesList), dtype = object)
imageMethods = ImageProcessingMethods()
imageSplittingMethods = ImageSegmentationMethods()
foundCharsList = []


for n in range(0, len(filesList)):
    images[n] = cv2.imread(join('./captchasLines/', filesList[n]))
    #plt.imshow(images[n])
    #plt.show()
    
for i in range(0, len(images)):
    methodSelector = 2
    
    # Determine type of captcha
    
    if methodSelector == 0:
        preprocessedImg = imageMethods.connectedCharacterSeperation(images[i])
        foundCharsList = imageSplittingMethods.getCharacters(imageMethods.segmentationPrep(preprocessedImg))
        #plt.imshow(preprocessedImg)
        #plt.show()
        
        
        if len(foundCharsList) < 4:
            preprocessedImg = imageMethods.heavyNoiseRemovalFilter(images[i])
            foundCharsList = imageSplittingMethods.getCharacters(preprocessedImg)
            #plt.imshow(preprocessedImg)
            #plt.show()
    
    elif methodSelector == 1:
        preprocessedImg = imageMethods.heavyNoiseRemovalFilter(images[i])
        foundCharsList = imageSplittingMethods.getCharacters(preprocessedImg)
        #plt.imshow(preprocessedImg)
        #plt.show()
        
    elif methodSelector == 2:
        preprocessedImg = imageMethods.lineNoiseCharacterSeperation(images[i])
                
        chars, foundList = imageSplittingMethods.getCharacters(imageMethods.segmentationPrep(preprocessedImg))
        imageSplittingMethods.sortCharacters(foundList)
    #foundCharsList = imageSplittingMethods.getCharacters(initThreshPreProcessedImg)


    #chars = measure.label(initThreshPreProcessedImg, neighbors = 8, background = 1) # identify seperate parts of img, bg has chars[x, y] = 1
    # Iterate through identified parts of img 
    #for char in np.unique(chars):
        
        # If less than 4 or greater than max boundary of 12 identified parts pre-procssing has not correctly cleaned image, use different technique
     #   if (char == (len(np.unique(chars)) - 1)) and characterFoundCount < 4:
      #      print('new technique needed')

        # If == 1 then pixel is part of background and no interest
       # if char == 1:
        #    continue
        
        # Create a mask to store each seperate char, also includes holes in characters such as 8 
       # mask = np.zeros(initThreshPreProcessedImg.shape, dtype = 'uint8')
        #mask[chars == char] = 255
        
        # Find the contours of the character (borders)
        #contImg, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) # Chain aprrox simple sves alot of memory
        
        # If no contours then not a character
        #if len(contours) > 0:
            
            # Get outermost contour as to not include holes
         #   primaryContour = max(contours, key = cv2.contourArea)
          #  (X, Y, width, height) = cv2.boundingRect(primaryContour)
           # 
            ## Calculate Solidarity and hieght ratio of character to determine whether is truely a character or just noise/hole
#            solidarity = (cv2.contourArea(primaryContour) / float(width * height)) # Ratio between contour size and size of character, if large then probably a character
 #           heightRatio = height / float(initThreshPreProcessedImg.shape[0]) # RAtio between height of character and height of original image, if large then probably character
  #          widthRatio = width / float(initThreshPreProcessedImg.shape[1])
   #         
    #        verifiedSolidarity = solidarity > 0.15
     #       verifiedHeight = heightRatio > 0.5 
      #      verifiedWidth = widthRatio < 0.3
       #     
        #    if verifiedHeight and verifiedSolidarity and verifiedWidth:
         #       characterFoundCount = characterFoundCount + 1
          #      
           #     charObjects = np.zeros(initThreshPreProcessedImg.shape, dtype = 'uint8') # Create blank 2D array to store generated character hull 
            #    splitObject = np.zeros(initThreshPreProcessedImg.shape, dtype = 'uint8') # Create blank 2D array to store separated captcha character
             #   
                # Generate hull for each character
                
#                hull = cv2.convexHull(primaryContour)
 #               cv2.drawContours(charObjects, [hull], -1, 255, -1)
  #              
  #             rows = charObjects.shape[0]
   #             cols = charObjects.shape[1]
    #            
     #           # Only interested in pixels within the hull's as these pixels make up the charcters, hulls also include holes which we dont want. Logic excludes holes.
      #          
       #         for xVal in range(0, rows):
        #            for yVal in range(0, cols):
         #               if (chars[xVal, yVal] * charObjects[xVal, yVal]) == char*255:
          #                  splitObject[xVal, yVal] = charObjects[xVal, yVal]
           #                 
            #    finalCharsList.append(splitObject)
